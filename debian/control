Source: geventhttpclient
Section: python
Priority: optional
Maintainer: Sandro Tosi <morph@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all-dev,
               python3-brotli <!nocheck>,
               python3-certifi,
               python3-dpkt <!nocheck>,
               python3-gevent (>= 24.11.1),
               python3-pytest <!nocheck>,
               python3-setuptools,
               python3-six,
               python3-urllib3 <!nocheck>,
Standards-Version: 4.6.1
Homepage: https://github.com/gwik/geventhttpclient
Vcs-Git: https://salsa.debian.org/morph/geventhttpclient.git
Vcs-Browser: https://salsa.debian.org/morph/geventhttpclient
Testsuite: autopkgtest-pkg-pybuild

Package: python3-geventhttpclient
Architecture: any
Depends: ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends},
Recommends: ${python3:Recommends},
Suggests: ${python3:Suggests},
Description: high performance, concurrent HTTP client library for Python using gevent
 geventhttpclient uses a fast http parser, written in C, originating from
 nginx, extracted and modified by Joyent.
 .
 geventhttpclient has been specifically designed for high concurrency,
 streaming and support HTTP 1.1 persistent connections. More generally it is
 designed for efficiently pulling from REST APIs and streaming APIs
 like Twitter's.
 .
 Safe SSL support is provided by default. geventhttpclient depends on
 the certifi CA Bundle. This is the same CA Bundle which ships with the
 Requests codebase, and is derived from Mozilla Firefox's canonical set.
